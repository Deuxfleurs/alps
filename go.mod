module git.sr.ht/~migadu/alps

go 1.13

require (
	github.com/aymerick/douceur v0.2.0
	github.com/chris-ramon/douceur v0.2.0
	github.com/dustin/go-humanize v1.0.0
	github.com/emersion/go-ical v0.0.0-20220601085725-0864dccc089f
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-imap-metadata v0.0.0-20200128185110-9d939d2a0915
	github.com/emersion/go-imap-move v0.0.0-20210907172020-fe4558f9c872
	github.com/emersion/go-imap-specialuse v0.0.0-20201101201809-1ab93d3d150e
	github.com/emersion/go-message v0.16.0
	github.com/emersion/go-sasl v0.0.0-20220912192320-0145f2c60ead
	github.com/emersion/go-smtp v0.15.1-0.20221018181223-201c9ab124e4
	github.com/emersion/go-vcard v0.0.0-20220507122617-d4056df0ec4a
	github.com/emersion/go-webdav v0.3.2-0.20220531141108-9bc7a8f15b2f
	github.com/fernet/fernet-go v0.0.0-20211208181803-9f70042a33ee
	github.com/google/uuid v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.9.1
	github.com/labstack/gommon v0.4.0
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/microcosm-cc/bluemonday v1.0.21
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	github.com/teambition/rrule-go v1.8.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20220504180219-658193537a64
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/net v0.0.0-20221012135044-0b7e1fb9d458
	golang.org/x/sys v0.0.0-20221010170243-090e33056c14 // indirect
	golang.org/x/text v0.3.8 // indirect
	golang.org/x/time v0.0.0-20220922220347-f3bd1da661af // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	jaytaylor.com/html2text v0.0.0-20211105163654-bc68cce691ba
	layeh.com/gopher-luar v1.0.10
)
